require 'json'
require 'net/http'
require 'uri'

artifact_relative_path = ENV['CI_PROJECT_DIR'] + '/semgrep-scg.json'

json_data = File.read(artifact_relative_path)

data = JSON.parse(json_data)

path_line_message_dict = {}

results = data["results"]
results.each do |result|
  line = result["start"]["line"]
  path = result["path"]
  message = result["extra"]["message"]
  
  path_line_message_dict[path] = { line: line, message: message }
end

puts "old dict: #{path_line_message_dict}"

# Code below is to ensure duplicate comments are not created when an MR is updated.
# We parse all the comments created by the bot, and parse the filename, line and
# message. If this match exists, we delete the key from path_line_message_dict 
# which is used to create the comments

discussions_url = URI.parse("#{ENV['CI_API_V4_URL']}/projects/#{ENV['CI_MERGE_REQUEST_PROJECT_ID']}/merge_requests/#{ENV['CI_MERGE_REQUEST_IID']}/discussions")

request = Net::HTTP::Get.new(discussions_url)
request["PRIVATE-TOKEN"] = ENV['PAT']

response = Net::HTTP.start(discussions_url.hostname, discussions_url.port, use_ssl: discussions_url.scheme == 'https') do |http|
    http.request(request)
  end

comments = JSON.parse(response.body)

comments.each do |comment|
  notes = comment['notes']

  notes.each do |note|
    if note['author']['id'] == ENV['BOT_USER_ID']
      if path_line_message_dict[note['position']['new_path']] == { line: note['position']['new_line'], message: note['body'] }
        path_line_message_dict.delete(note['position']['new_path'])
      end
    end
  end
end

puts "new dict: #{path_line_message_dict}"

versions_url = URI.parse("#{ENV['CI_API_V4_URL']}/projects/#{ENV['CI_MERGE_REQUEST_PROJECT_ID']}/merge_requests/#{ENV['CI_MERGE_REQUEST_IID']}/versions")

request = Net::HTTP::Get.new(versions_url)
request["PRIVATE-TOKEN"] = ENV['PAT']

response = Net::HTTP.start(versions_url.hostname, versions_url.port, use_ssl: versions_url.scheme == 'https') do |http|
    http.request(request)
  end

commits = JSON.parse(response.body)[0]

base_sha = commits['base_commit_sha']
head_sha = commits['head_commit_sha']
start_sha = commits['start_commit_sha']

path_line_message_dict.each do |path, info|
puts "#{path} has issue on line #{info[:line]}: #{info[:message]}}"
  new_path = old_path = path  # Keeping both new_path and old_path as the full path
  new_line = old_line = info[:line]  # Setting new_line and old_line to the line number
  message = info[:message]  # Getting the message

  # Constructing the URI for the HTTP request
  uri = URI.parse(ENV['CI_API_V4_URL'] + '/projects/' + ENV['CI_MERGE_REQUEST_PROJECT_ID'] + '/merge_requests/' + ENV['CI_MERGE_REQUEST_IID'] + '/discussions')

  # Constructing the request
  request = Net::HTTP::Post.new(uri)
  request["PRIVATE-TOKEN"] = ENV['PAT']
  request.set_form_data(
    "position[position_type]" => "text",
    "position[base_sha]" => base_sha,
    "position[head_sha]" => head_sha,
    "position[start_sha]" => start_sha,
    "position[new_path]" => new_path,
    "position[old_path]" => old_path,
    "position[new_line]" => new_line,
    "body" => message
  )

  # puts "#{request.method} #{uri}"

  # Print out headers and body
  # request.each_header do |header, value|
  # puts "#{header}: #{value}"
  # end

  # puts request.body
  
  # Sending the request
  response = Net::HTTP.start(uri.hostname, uri.port, use_ssl: uri.scheme == 'https') do |http|
    http.request(request)
  end

  # Printing the response
  # puts response.body
end